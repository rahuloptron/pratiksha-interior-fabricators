<?php
   include 'currenturl.php';
?>
<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Contact Us - Pratiksha Interior</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
   
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?><!-- end header -->

<div class="clearfix"></div>
<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>Contact Us</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">
        	
    <div class="one_half">
        

    <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
    <br />
    <p>Please be patient while waiting for response. (10:00 AM - 06:00 PM Support!)
    <strong>Phone General Inquiries: +91 9867 12 03 09</strong></p>

    <br />
    <h3><i>Contact Form</i></h3>

    <form action="#" method="post">
    
        <fieldset>
        
                
        <label for="name" class="blocklabel">Your Name*</label>
        <p class="" ><input name="yourname" class="input_bg" type="text" id="name" value=''/></p>
        
        
        <label for="email" class="blocklabel">E-Mail*</label>
        <p class="" ><input name="email" class="input_bg" type="text" id="email" value='' /></p>
        
        
        <label for="website" class="blocklabel">Website</label>
        <p><input name="website" class="input_bg" type="text" id="website" value=""/></p>
        
        
        <label for="message" class="blocklabel">Your Message*</label>
        <p class=""><textarea name="message" class="textarea_bg" id="message" cols="20" rows="7" ></textarea></p>
        
        
        <p>
        <input type="hidden" id="myemail" name="myemail" value="gsrthemes9@gmail.com" />
        <input type="hidden" id="myblogname" name="myblogname" value="yourcompanyname.com" />
        <div class="clearfix"></div>
        <input name="Send" type="submit" value="SUBMIT" class="comment_submit" id="send"/></p>
                
        </fieldset>
        
        </form> 
    
    </div>
               
    <div class="one_half last">
    
        <div class="address-info">
            <h3><i>Address Info</i></h3>
                <ul>
                <li>
                <strong>PRATIKSHA INTERIORS & FABRICATORS</strong><br />
                4,SainathNivas,Motiram Compound, G.B.Rd ,Orlem,Malad West, Mumbai, 400064.<br />
                Telephone: +91 9867 12 03 09<br />
                E-mail: <a href="mailto:mail@companyname.com">pidc.ent@gmail.com</a>
                </li>
            </ul>
        </div>

         <h3><i>Find the Address</i></h3>
            <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.8905534938067!2d72.83405061447012!3d19.199982087016956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b6c3a463a6bb%3A0x703167a5e3a6b23c!2sShree+Balaji+Xerox+and+Telecom!5e0!3m2!1sen!2sin!4v1522124313608" width="550" height="350" frameborder="0" style="border:0" allowfullscreen></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=WA,+United+States&amp;aq=0&amp;oq=WA&amp;sll=47.605288,-122.329296&amp;sspn=0.008999,0.016544&amp;ie=UTF8&amp;hq=&amp;hnear=Washington,+District+of+Columbia&amp;t=m&amp;z=7&amp;iwloc=A">View Larger Map</a></small>
        
    </div>
            
</div>

</div><!-- end content area -->


<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>


</body>
</html>

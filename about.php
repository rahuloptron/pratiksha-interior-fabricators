<?php
   include 'currenturl.php';
?>

<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>About Us - Pratiksha Interior</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />

	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

   
    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- faqs -->
    <link rel="stylesheet" href="js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?>
<!-- end header -->

<div class="clearfix"></div>

<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>About Us</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">

    <h2><strong>Founder</strong></h2>  

  <p>Founder of PIF Mr. Jignesh Kawa is 15 Experienced Interior Designer & Consultant for Roofing, Walling & Cladding in civil and industrial construction. He Consulting & Providing Installation Service in many applications both exterior and interior i.e. Factory Building, Warehouses, Market, Vehicle Parking sheds, Farm House, Water Resorts, Cottages, Chemical Industries, Animal House, Construction House, Schools, Colleges, Hotels etc. </p>

</div>

        <div class="clearfix divider_line2"></div>
    

	<ul class="fullimage_box">
            <li><h2>Who <strong>We Are</strong></h2></li>
            <li><img src="images/who-we-are.jpg" alt=""></li>
            <li>About Us
            Pratiksha Interior & Fabricators (PIF) is one of the well-established Company, providing High quality rigid three layer Un-plasticized polyvinylchloride (uPVC) Roof sheet installation services. These services are managed by our most experienced and expert professionals to meet the specifications of patrons and international quality standards. We provide customized Installation as per customer’s requirement. The offered services are highly praised by the customers for their timely execution and reliability.
            </li>    
        </ul>

        <ul class="fullimage_box last">
            <li><h2>What <strong>We Do</strong>(save 70% of Construction Cost)</h2></li>
            <li><img src="images/what-we-do.jpg" alt=""></li>
            <li>We use advanced superior quality 3 layer Un-plasticized polyvinylchloride (uPVC) roofing sheets for Roofing, uPVC Form Board & WPC Board. This uPVC is the best replacement to conventional asbestos, metal G.I. sheets, fiberglass sheets, ceramic tile sheets and uPVC sheets considering the heat insulation, sound insulation, water resistance, chemical and alkali resistance, corrosion proof, fire retardant, eco friendly, easy to install and move, light weight as well as cost effective in installation and it can save construction cost up to 70%. As per the efficiency and benefits it provides.</li>    
        </ul>

        

</div>



</div><!-- end content area -->


<div class="clearfix mar_top7"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<!-- accordion -->
<script type="text/javascript" src="js/accordion/custom.js"></script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>


</body>
</html>

<?php
   include 'currenturl.php';
?>
<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Portfolio - Pratiksha Interior</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

  
    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- fancyBox -->
    <link rel="stylesheet" type="text/css" href="js/portfolio/source/jquery.fancybox.css" media="screen" />
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?><!-- end header -->

<div class="clearfix"></div>

<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>Our Work</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">
    
		<h2>UPVC <strong>ROOFING SHEETS </strong></h2>
        
		<div class="one_third">
        	<div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/7.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/7-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
        	<div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/12.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/12-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
        	<div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/16.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/16-1.jpeg" alt="" /></a>
            </div> 
        </div><!-- end section -->
        
        <div class="clearfix mar_top5"></div>
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/20.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/20-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/22.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/22-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/25.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/25-1.jpeg" alt="" /></a>
            </div> 
        </div><!-- end section -->


    
	</div>

    <div class="content_fullwidth">
    
        <h2>ASA UPVC <strong>ROOF TILES SHEETS </strong></h2>
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/tiles-sheets/b.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/b-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/tiles-sheets/e.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/e-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/tiles-sheets/m.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/m-1.jpeg" alt="" /></a>
            </div> 
        </div><!-- end section -->
        
        <div class="clearfix mar_top5"></div>
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
           <a class="fancybox" href="images/work/tiles-sheets/n.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/n-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/tiles-sheets/l.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/l-1.jpeg" alt="" /></a>
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
            <div class="portfolio_image">
            <i class="icon-search icon-4x"></i>
            <a class="fancybox" href="images/work/tiles-sheets/j.jpeg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/work/tiles-sheets/j-1.jpeg" alt="" /></a>
            </div> 
        </div><!-- end section -->

        
    
    </div>

</div><!-- end content area -->


<div class="clearfix mar_top7"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>

<!-- fancyBox -->
<script type="text/javascript" src="js/portfolio/lib/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="js/portfolio/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/portfolio/source/helpers/jquery.fancybox-media.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        /* Simple image gallery. Uses default settings */
        $('.fancybox').fancybox();

        /* media effects*/  
        $(document).ready(function() {
            $('.fancybox-media').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                helpers : {
                    media : {}
                }
            });
        });

    });
</script>

</body>
</html>

<?php
   include 'currenturl.php';
?>
<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>UPVC Transparent Profile - Pratiksha Interior</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">

    <!-- testimonials -->
    <link rel="stylesheet" href="js/testimonials/fadeeffect.css" type="text/css" media="all">

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?>
<!-- end header -->

<div class="clearfix"></div>


<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>UPVC Transparent Profile</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">

       <div class="one_half">

        <img src="images/trapezoidal-wave-profile.png" class="image_left1">
           
       </div>

       <div class="one_half last">
           
           <ul>
               
            <li>Product Code: TTW-1130 (Transparent Trapezoidal Wave Profile)</li>
            <li>Product Code: TRW-1130 (Transparent Round Wave Profile)</li>
            <li>Thickness: 1.0, 1.5 (mm.)</li>
            <li>Width: 1130 mm. (1.13mtr.)</li>
            <li>Length: 2.0, 2.5, 3.0 (mtr.)</li>
            <li>Also customizable as per customer’s required on bulk order only</li>
            <li>Color: White, Blue, Green</li>

           </ul>

        <div class="clearfix mar_top10"></div>
           <div class="clearfix mar_top8"></div>

       </div>

    <div class="clearfix divider_line2"></div>

    <h2>uPVC <strong>Material Structure</strong></h2>

        <div class="one_full">
            
        <div class="one_third">
    
        <ul class="fullimage_box2">
           
            <li><h3>Surface Layer</h3></li>
            <li>The Material is ASA(Acrylonitrile styrene acrylate) and ultra weather resin, to block the solar ultra violet and reduce the coefficient of heat conductivity, ensure product durability and resistance to chemical corrosion.</li>
        </ul>
    
        </div>
        
        <div class="one_third">
    
        <ul class="fullimage_box2">
           
            <li><h3>Intermediate Layer</h3></li>
            <li>Contains Multi foam giving to the material light weight and improved sound and thermal insulating properties.</li>
        </ul>
    
    </div>
        
        <div class="one_third last">
    
        <ul class="fullimage_box2">
           
            <li><h3>Underlying layer</h3></li>
            <li>contains Multi foam giving to the material light weight and improved sound and themal insulating properties and its a good material toughness special to ensure the strength at the same time, increase the rigidity and light illumination.</li>
        </ul>
    
    </div>
        
    </div>
    
    <div class="clearfix divider_line8"></div>

   <div class="one_full">
    
        <h2><strong>Technical Specification</strong></h2>
            <div class="table-style">
                <table class="table-list">
                    <tbody><tr>
                        <th>Property</th>
                        <th>Test Method</th>
                        <th>Value</th>
                       
                    </tr>
                    <tr>
                        <td>Density</td>
                        <td>ASTM D 792</td>
                        <td>1.47 gm/cc</td>
                       
                    </tr>
                    <tr>
                        <td>Softening temp. at 10 N</td>
                        <td>ASTM D 1525</td>
                        <td>90.6 °c</td>
                       
                    </tr>
                    <tr>
                        <td>HDT 66 PSI</td>
                        <td>ASTM D 648</td>
                        <td>71.9 °c</td>
                        
                    </tr>

                    <tr>
                        <td>HDT 264 PSI</td>
                        <td>-</td>
                        <td>66.2 °c</td>
                        
                    </tr>

                    <tr>
                        <td>Rate of Burning</td>
                        <td>ASTM D 648</td>
                        <td><5 mm</td>
                        
                    </tr>

                    <tr>
                        <td>Tensile Strength at break</td>
                        <td>TASTM D 638</td>
                        <td>410 Kg/cm2</td>
                        
                    </tr>

                    <tr>
                        <td>Elongation at break</td>
                        <td>ASTM D 638</td>
                        <td>32%</td>
                        
                    </tr>

                    <tr>
                        <td>Oxygen index</td>
                        <td>ASTM D 2863</td>
                        <td>38%</td>
                        
                    </tr>

                    <tr>
                        <td>Flammability</td>
                        <td>UI 94</td>
                        <td>Match with V°</td>
                        
                    </tr>

                    <tr>
                        <td>Water Absorption</td>
                        <td>ASTM D 570</td>
                        <td>0.07%</td>
                        
                    </tr>
                    
                </tbody></table>

            </div>
    
    </div>

    <div class="clearfix divider_line3"></div>


        <div class="clients">

    <div class="container">
    
        <ul id="mycarouselthree" class="jcarousel-skin-tango">
          
            <li><img src="images/client-logo/indiana-group-logo.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-thomas.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-galaxy.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-goodluck.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-onerio.jpg" alt=""></li>

            <li><img src="images/client-logo/logo-dudheshwerhealthresort.png" alt=""></li>

            <li><img src="images/client-logo/logo-supriya-life-science.png" alt=""></li>

            <li><img src="images/client-logo/logo-bakul-pharma.jpg" alt=""></li>

          </ul>
    
    </div>

</div><!-- end clients -->

    <div class="clearfix divider_line3"></div>

    <div class="punchline_text_box">
        <div class="left">
        <strong>Want to start something new with us ?</strong>
        <p>The Pratriksh Interior is a perfect blend of a creative agency and the best practices of the digital world. We provide digital promotion services by raising consumer awareness of a product or brand, generating sales, and thus creating a long term brand loyalty.</p>
       
        </div>
        <div class="right"><a href="contact.html" class="knowmore_but"><i class="fa fa-hand-o-right fa-lg"></i> Know&nbsp;More</a></div>
    </div>

     
</div>

</div><!-- end content area -->


<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">

    jQuery(document).ready(function() {
            jQuery('#mycarousel').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouseltwo').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselthree').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselfour').jcarousel();
    });
    
</script>

<!-- testimonials -->
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
    
$(".controlls li a").click(function(e) {
    e.preventDefault();
    var id = $(this).attr('class');
    $('#slider div:visible').fadeOut(500, function() {
        $('div#' + id).fadeIn();
    })
});
});//]]>  

</script>



<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>


</body>
</html>

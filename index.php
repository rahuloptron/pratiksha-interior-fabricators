<?php
   include 'currenturl.php';
?>
<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
    <title>Interior Designer</title>
    
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />

    
<?php include 'includes/css.php' ?>
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?>
<!-- end header -->
   

<div class="clearfix"></div>
 
<!-- Slider
======================================= -->  

<div class="container_full">
    
    <div class="fullwidthbanner-container">
    
        <div class="fullwidthbanner">
        
                        <ul>    
                     
                            <li data-transition="papercut" data-slotamount="9" data-thumb="images/sliders/revolution/slider-bg3.jpg">
                                
                                <img src="images/sliders/revolution/slider-bg6.jpg" alt="" />  

                                <div class="caption lft big_white"  data-x="0" data-y="80" data-speed="900" data-start="700" data-easing="easeOutExpo"><div class="display_text_center">Find the Benefits of Using Pratiksha Interior. &amp; Recommended Highly</div></div>
                                
                                <div class="caption lft large_text"  data-x="0" data-y="139" data-speed="900" data-start="1300" data-easing="easeOutExpo"><div class="display_text_center">We Build Smarter Stuff!</div></div>

                               
                            </li>

                            <li data-transition="papercut" data-slotamount="9" data-thumb="images/sliders/revolution/slider-bg3.jpg">
                                
                                <img src="images/sliders/revolution/slider-bg7.jpg" alt="" />  

                                <div class="caption lft big_white"  data-x="0" data-y="80" data-speed="900" data-start="700" data-easing="easeOutExpo"><div class="display_text_center">Find the Benefits of Using Pratiksha Interior. &amp; Recommended Highly</div></div>
                                
                                <div class="caption lft large_text"  data-x="0" data-y="139" data-speed="900" data-start="1300" data-easing="easeOutExpo"><div class="display_text_center">We Build Smarter Stuff!</div></div>
                                
                               
                            </li>

                            <li data-transition="papercut" data-slotamount="9" data-thumb="images/sliders/revolution/slider-bg3.jpg">
                                
                                <img src="images/sliders/revolution/slider-bg8.jpg" alt="" />  

                                <div class="caption lft big_white"  data-x="0" data-y="80" data-speed="900" data-start="700" data-easing="easeOutExpo"><div class="display_text_center">Find the Benefits of Using Pratiksha Interior. &amp; Recommended Highly</div></div>
                                
                                <div class="caption lft large_text"  data-x="0" data-y="139" data-speed="900" data-start="1300" data-easing="easeOutExpo"><div class="display_text_center">We Build Smarter Stuff!</div></div>
                                
                               
                            </li>
                                
                        </ul>
                        
                    </div>
                    
                </div>


</div><!-- end slider -->


<div class="clearfix mar_top6"></div>

<div class="four_col_fusection container">

    <div class="one_fourth">
        
     
        <div class="clearfix"></div>
        
        <h2>UPVC Roofing</h2>
        
        <p>Trapezoidal wave profiles are made from high qualitative UPVC (Un- plasticized Polyvinyl Chloride) which are exactly made-up with the latest production techniques</p>
        <br />
        <a href="/upvc-trapezoidal-wave-profile.html" class="but_download">Read More <i class="icon-angle-right"></i></a>
    </div><!-- end section -->
    
    <div class="one_fourth">
        
        <div class="clearfix"></div>
        
        <h2>UPVC Tile Sheets</h2>
        
         <p>Trapezoidal wave profiles are made from high qualitative UPVC (Un- plasticized Polyvinyl Chloride) which are exactly made-up with the latest production techniques</p>
        <br />
        <a href="/asa-upvc-tile-sheet.html" class="but_download">Read More <i class="icon-angle-right"></i></a>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
        
        <div class="clearfix"></div>
        
        <h2>WPC Boards</h2>
        
         <p>Trapezoidal wave profiles are made from high qualitative UPVC (Un- plasticized Polyvinyl Chloride) which are exactly made-up with the latest production techniques</p>
        <br />
        <a href="/wpc-boards.html" class="but_download">Read More <i class="icon-angle-right"></i></a>
    
    </div><!-- end section -->
    
    <div class="one_fourth last">
        
        <div class="clearfix"></div>
        
        <h2>PVC Foam Sheet</h2>
        
         <p>Trapezoidal wave profiles are made from high qualitative UPVC (Un- plasticized Polyvinyl Chloride) which are exactly made-up with the latest production techniques</p>
        <br />
        <a href="/pvc-foam-sheet.html" class="but_download">Read More <i class="icon-angle-right"></i></a>
    
    </div><!-- end section -->

</div>

<div class="clearfix mar_top6"></div>

<div class="fresh_projects">
    <div class="clearfix mar_top6"></div>
    
    <div class="container">
    
        <h1>Latest <strong>Projects</strong> </h1>
        
        <ul id="mycarouseltwo" class="jcarousel-skin-tango">
          
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/tiles-sheets/b.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/7.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/1.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/16.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/25.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/tiles-sheets/e.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/tiles-sheets/m.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->

            <li>
                <div class="item">                        
                <div class="fresh_projects_list">
                    <section class="cheapest">
                        <div class="display">                  
                            <div class="small-group">        
                                <div class="small money">  
                                    
                                        <img src="images/work/tiles-sheets/n.jpeg" alt="">
                                       
                                        <div class="hover"></div>
                                   
                                </div>        
                            </div>     
                        </div>
                    </section>
                </div>
                </div>
            </li><!-- end item -->
            
            
          </ul>
        
    
    </div>
    
    <div class="clearfix mar_top3"></div>
    
</div><!-- end fresh projects -->


<div class="clearfix mar_top4"></div>

<div class="clients">

    <div class="container">
    
        <ul id="mycarouselthree" class="jcarousel-skin-tango">
          
            <li><img src="images/client-logo/indiana-group-logo.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-thomas.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-galaxy.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-goodluck.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-onerio.jpg" alt=""></li>

            <li><img src="images/client-logo/logo-dudheshwerhealthresort.png" alt=""></li>

            <li><img src="images/client-logo/logo-supriya-life-science.png" alt=""></li>

            <li><img src="images/client-logo/logo-bakul-pharma.jpg" alt=""></li>

          </ul>
    
    </div>

</div><!-- end clients -->
<!-- 
<div class="clearfix divider_line2"></div>

    <div class="punchline_text_box">
        <div class="left">
        <strong>Want to start something new with us ?</strong>
        <p>The Pratriksh Interior is a perfect blend of a creative agency and the best practices of the digital world. We provide digital promotion services by raising consumer awareness of a product or brand, generating sales, and thus creating a long term brand loyalty.</p>
       
        </div>
        <div class="right"><a href="contact.html" class="knowmore_but"><i class="fa fa-hand-o-right fa-lg"></i> Know&nbsp;More</a></div>
    </div> -->

<div class="clearfix mar_top4"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->



 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->

<?php include 'includes/js.php' ?>
</body>
</html>

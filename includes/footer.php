<!-- <div class="footer">

	<div class="arrow_02"></div>
	
    <div class="clearfix mar_top5"></div>
	
    <div class="container">
    
   		<div class="one_fourth">
            
        
            
            <ul class="contact_address">
                <li><p><strong>PRATIKSHA INTERIORS & FABRICATORS</strong></p></li>
                <li><i class="icon-map-marker icon-large"></i>&nbsp; 4,SainathNivas,Motiram Compound, G.B.Rd ,Orlem,Malad West, Mumbai, 400064.</li>
                <li><i class="icon-phone"></i>&nbsp; +91 9867 12 03 09</li>
                <li><i class="icon-envelope"></i>&nbsp; pidc.ent@gmail.com</li>
                
            </ul>
            
        </div>
        
        <div class="one_fourth">
        	
            <h2>Useful <i>Links</i></h2>
            
            <ul class="list">
                <li><a href="#"><i class="icon-angle-right"></i> Trapezoidal Wave Profile</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Transparent Profile</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> ASA UPVC Tile Sheets </a></li>
                <li><a href="#"><i class="icon-angle-right"></i> WPC Boards</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> PVC Foam Sheet</a></li>

              
            </ul>
            
        </div>
         
         <div class="one_fourth">
            
            <h2>Useful <i>Links</i></h2>
            
            <ul class="list">
                <li><a href="#"><i class="icon-angle-right"></i> Home Page Variations</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Awsome Slidershows</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Features and Typography</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Different &amp; Unique Pages</a></li>
                
            </ul>
            
        </div>
        
        
        <div class="one_fourth last">
        	
            <h2>Useful <i>Links</i></h2>
            
            <ul class="list">
                <li><a href="#"><i class="icon-angle-right"></i> Home Page Variations</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Awsome Slidershows</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Features and Typography</a></li>
                <li><a href="#"><i class="icon-angle-right"></i> Different &amp; Unique Pages</a></li>
               
            </ul>
            
        </div>
        
        
    </div>
	
    <div class="clearfix mar_top5"></div>
    
</div>
 -->

<div class="copyright_info">

    <div class="container">
    
        <div class="one_half">
        
            <b>Copyright © 2018 Pratiksha Interior. All rights reserved.  <a href="terms.html">Terms of Use</a> | <a href="privacy-policy.html">Privacy Policy</a></b>
            
        </div>
    
    	
    
    </div>
    
</div><!-- end copyright info -->
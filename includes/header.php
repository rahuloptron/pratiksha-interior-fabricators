<header id="header">

	<!-- Top header bar -->
	<div id="topHeader">
    
	<div class="wrapper">
         
        <div class="top_contact_info">
        
        <div class="container">
        
          
        
            <ul class="tci_list">
            
                <li class="empty"><i class="icon-phone-sign"></i> +91 9867 12 03 09</li>
                <li class="empty"><a href="mailto:info@anova.com"><i class="icon-envelope"></i> pidc.ent@gmail.com</a></li>
                
               <!--  <li><a href="#"><i class="icon-facebook"></i></a></li>
                <li><a href="#"><i class="icon-twitter"></i></a></li>
                <li><a href="#"><i class="icon-google-plus"></i></a></li>
                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                <li><a href="#"><i class="icon-youtube"></i></a></li> -->
             
            </ul>
            
        </div>
        
    </div><!-- end top contact info -->
            
 	</div>
    
	</div>
	
    
	<div id="trueHeader">
    
	<div class="wrapper">
    
     <div class="container">
    
		<!-- Logo -->
		<div class="one_fourth"><a href="index.html" id="logo"></a></div>
		
        <!-- Menu -->
        <div class="three_fourth last">
           
           <nav id="access" class="access" role="navigation">
           
            <div id="menu" class="menu">
                
                <ul id="tiny">

                    <li> <a <?php if($currentURL == "/index.php") echo "class='active'"; ?> href="/index.html">Home</a>
                    </li>

                    <li> <a <?php if($currentURL == "/about.php") echo "class='active'"; ?> href="/about.html">About Us</a>
                    </li>
                
                    <li><a href="#">Product<i class="icon-angle-down"></i></a>
                    
                        <ul>
                            <li><a href="#">UPVC Roofing Sheets<i class="icon-angle-down"></i></a>
                                    <ul>
                                       
                                         <li> <a <?php if($currentURL == "/upvc-trapezoidal-wave-profile.php") echo "class='active'"; ?> href="/upvc-trapezoidal-wave-profile.html">Trapezoidal Wave Profile</a>
                                        </li>


                                         <li> <a <?php if($currentURL == "/upvc-transparent-profile.php") echo "class='active'"; ?> href="/upvc-transparent-profile.html">Transparent Profile</a>
                                        </li>

                                    </ul>

                            </li>
                         

                    <li> <a <?php if($currentURL == "/asa-upvc-tile-sheet.php") echo "class='active'"; ?> href="/asa-upvc-tile-sheet.html">ASA UPVC Tile Sheets</a>
                    </li>

                    <li> <a <?php if($currentURL == "/wpc-boards.php") echo "class='active'"; ?> href="/wpc-boards.html">WPC Boards</a>
                    </li>

                    <li> <a <?php if($currentURL == "/pvc-foam-sheet.php") echo "class='active'"; ?> href="/pvc-foam-sheet.html">PVC Foam Sheet</a>
                    </li>
                    
                    </ul>
                        
                    </li>
  
                    <li> <a <?php if($currentURL == "/portfolio.php") echo "class='active'"; ?> href="/portfolio.html">Our Work</a>
                    </li>
                    
                    <li class="last"> <a <?php if($currentURL == "/contact.php") echo "class='active'"; ?> href="/contact.html">Contact Us</a>
                    </li>
                
  
                </ul>
                
            </div>
            
        </nav><!-- end nav menu -->
      
        </div>
        
        
		</div>
		
	</div>
    
	</div>
    
</header>
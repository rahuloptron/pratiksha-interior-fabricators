<?php
   include 'currenturl.php';
?>

<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>ASA UPVC TILE SHEET - Pratiksha Interior</title>
	
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
   
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">

    <!-- testimonials -->
    <link rel="stylesheet" href="js/testimonials/fadeeffect.css" type="text/css" media="all">

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
    
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?>
<!-- end header -->

<div class="clearfix"></div>

<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>ASA UPVC TILE SHEET</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">

        <div class="big_text1">We are supplying & Installing Latest uPVC Sheet for providing absolute roofing solution for Industrial, Commercial, Residential & Agriculture.</div>

        <div class="clearfix mar_top3"></div>

        <div class="one_full">
            
        <div class="one_third">
    
        <ul class="fullimage_box2">
           
            <li><h3>Surface Layer</h3></li>
            <li>The Material is ASA(Acrylonitrile styrene acrylate) and ultra weather resin, to block the solar ultra violet and reduce the coefficient of heat conductivity, ensure product durability and resistance to chemical corrosion.</li>
        </ul>
    
        </div>
        
        <div class="one_third">
    
        <ul class="fullimage_box2">
           
            <li><h3>Intermediate Layer</h3></li>
            <li>Contains Multi foam giving to the material light weight and improved sound and thermal insulating properties.</li>
        </ul>
    
    </div>
        
        <div class="one_third last">
    
        <ul class="fullimage_box2">
           
            <li><h3>Underlying layer</h3></li>
            <li>contains Multi foam giving to the material light weight and improved sound and themal insulating properties and its a good material toughness special to ensure the strength at the same time, increase the rigidity and light illumination.</li>
        </ul>
    
    </div>
        
    </div>

    
    <div class="clearfix divider_line2"></div>

	
    <div class="get_features">

           <h2>Product <strong>Features</strong></h2>
    
        <div class="clearfix mar_top3"></div>

            <div class="one_third">
            
                <ul class="get_features_list">
                    <li class="left"><i class="icon-coffee icon-2x"></i></li>
                    <li class="right">
                    <h5>Environmental protection and energy conservation:</h5>
                    <p>It is a kind of environmental-friendly, energy conservation and recycling building material popular in developed countries.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-file-alt icon-2x"></i></li>
                    <li class="right">
                    <h5>Light Weight</h5>
                    <p>The weight is 3.5 kg per square meters (2mm). Belonging to light structural material, it can effectively reduce the load of the building and save the assumption of building framework materials. In the mean time, it is easy to be handled and lifted, which reduces the construction cost.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-fullscreen icon-2x"></i></li>
                    <li class="right">
                    <h5>Pressure Prevention:</h5>
                    <p>DION sheets has very good load bearing performance. No damaged when 150kg is evenly loaded with the support distance being 750mm.</p></li>
                </ul>
                
            </div>
            
            <div class="one_third">
            
               
                <ul class="get_features_list">
                    <li class="left"><i class="icon-th icon-2x"></i></li>
                    <li class="right">
                    <h5>Corrosion Resistance</h5>
                    <p>DION sheets can resist the chemical corrosion of acid, alkali, salt etc. for a long time. The test proves that there is no chemical reaction when dipped in the salt, alkali etc.</p></li>
                </ul>

                 <ul class="get_features_list">
                    <li class="left"><i class="icon-resize-full icon-2x"></i></li>
                    <li class="right">
                    <h5>Sound & Heat Insulation:</h5>
                    <p>The coefficient of heat conductivity is 0.104 W/m.0 C, which is about 1/3 of clay tile, 1/5 of 10-mm-thick cement tile, and 1/2000 of 0.5 mm-thick color-coated steel tile. It can well absorb the noise when influenced by storm, hail and windstorm etc.</p></li>
                </ul>
                
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-crop icon-2x"></i></li>
                    <li class="right">
                    <h5>Good water resistance</h5>
                    <p>The surface material of DION is compact and doesn’t absorb water without the problem of water penetration by micro pores. The single sheet is big and there is very few joint on the roof. The connecting point is tightly connected; therefore the sheet has the outstanding water resistance performance.</p></li>
                </ul>

            </div>
            
            <div class="one_third last">
            
                <ul class="get_features_list">
                    <li class="left"><i class="icon-laptop icon-2x"></i></li>
                    <li class="right">
                    <h5>Flame Retardant</h5>
                    <p>Self extinguishing, the main body material polyvinyl chloride resin and its chemical performance determine that it is difficult to be burned.</p></li>
                </ul>
                
                
                <ul class="get_features_list last">
                    <li class="left"><i class="icon-align-justify icon-2x"></i></li>
                    <li class="right">
                    <h5>Cost effective & Non-Carcinogenic:</h5>
                    <p>Cost effective in installation and save construction up to 70%, overlapping waste doesn’t exceed 8%. High utilization efficiency upto 95% and its good for life fully protected from health hazards.</p></li>
                </ul>
                
            </div>
 
        </div>
    
   <div class="clearfix divider_line8"></div>

   <div class="one_full">
    
        <h2><strong>Technical Specification for uPVC Sheets</strong></h2>
            <div class="table-style">
                <table class="table-list">
                    <tbody><tr>
                        <th>Profile Name</th>
                        <th>Thickness (MM)</th>
                        <th>Standard Length</th>
                        <th>Color</th>
                    </tr>
                    <tr>
                        <td>Trapezoidal Wave Profile</td>
                        <td>1.0, 1.5, 2.0, 2.5, 3.0</td>
                        <td>8,10, 12 Feets</td>
                        <td>Blue, White, Green, Gray, Terracotta</td>
                    </tr>
                    <tr>
                        <td>Round Wave Profile</td>
                        <td>1.5, 2.0</td>
                        <td>8,10, 12 Feets</td>
                        <td>Blue, White, Green, Gray, Terracotta</td>
                    </tr>
                    <tr>
                        <td>ASA uPVC TILE SHEET</td>
                        <td>2.5</td>
                        <td>2.41,3.07,3.73 Mtr.</td>
                        <td>Terracotta</td>
                    </tr>
                    
                </tbody></table>


                <p><strong>Note:</strong> Length can be customized as per customers required on bulk order only. Purlin Span :1.5 mm < 1.5 Feet, 2 mm < 2 Feet, 2.5 mm < 2.5 Feet, 3 mm < 3 Feet.</p>

                <p><strong>Weight (Approx) :</strong> 2.6 kg per square meter (1.5 mm) 3.4 kg per square meter (2mm), 4 kg per square meter (2.5 mm), 5 kg per square meter (3 mm).</p>
            </div>
    
    </div>

    <div class="clearfix divider_line8"></div>

   <div class="one_full">
    
        <h2><strong>Technical Specification</strong></h2>
            <div class="table-style">
                <table class="table-list">
                    <tbody><tr>
                        <th>Property</th>
                        <th>Test Method</th>
                        <th>Value</th>
                       
                    </tr>
                    <tr>
                        <td>Density</td>
                        <td>ASTM D 792</td>
                        <td>1.47 gm/cc</td>
                       
                    </tr>
                    <tr>
                        <td>Softening temp. at 10 N</td>
                        <td>ASTM D 1525</td>
                        <td>90.6 °c</td>
                       
                    </tr>
                    <tr>
                        <td>HDT 66 PSI</td>
                        <td>ASTM D 648</td>
                        <td>71.9 °c</td>
                        
                    </tr>

                    <tr>
                        <td>HDT 264 PSI</td>
                        <td>-</td>
                        <td>66.2 °c</td>
                        
                    </tr>

                    <tr>
                        <td>Rate of Burning</td>
                        <td>ASTM D 648</td>
                        <td><5 mm</td>
                        
                    </tr>

                    <tr>
                        <td>Tensile Strength at break</td>
                        <td>TASTM D 638</td>
                        <td>410 Kg/cm2</td>
                        
                    </tr>

                    <tr>
                        <td>Elongation at break</td>
                        <td>ASTM D 638</td>
                        <td>32%</td>
                        
                    </tr>

                    <tr>
                        <td>Oxygen index</td>
                        <td>ASTM D 2863</td>
                        <td>38%</td>
                        
                    </tr>

                    <tr>
                        <td>Flammability</td>
                        <td>UI 94</td>
                        <td>Match with V°</td>
                        
                    </tr>

                    <tr>
                        <td>Water Absorption</td>
                        <td>ASTM D 570</td>
                        <td>0.07%</td>
                        
                    </tr>
                    
                </tbody></table>

            </div>
    
    </div>

    <div class="content_fullwidth">
    
        <h2><strong>Accessories</strong></h2>
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/accessories_1.jpg" alt="">
            <div class="title">Transparent Sheet</div>
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/accessories_2.jpg" alt="">
            <div class="title">Air Ventilator</div>
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/accessories_4.jpg" alt="">
            <div class="title">Fixer Set</div>
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth last">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/accessories_3.jpg" alt="">
            <div class="title">FRP Gutter</div>
            </div> 
        </div><!-- end section -->
        
    </div>


    <div class="content_fullwidth">
    
        <h2><strong>APPLICATIONS</strong></h2>
        <p>INDUSTRIAL, COMMERCIAL, RESIDENTIAL & AGRICULTURAL</p>
          <div class="clearfix mar_top3"></div>
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
           <img src="images/application_1.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_2.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_3.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth last">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_4.jpg" alt="">
            
            </div> 
        </div><!-- end section -->

         <div class="clearfix mar_top3"></div>

        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_5.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_6.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_7.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth last">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/application_8.jpg" alt="">
            
            </div> 
        </div><!-- end section -->
        
        
    </div>

    <div class="clearfix divider_line3"></div>


        <div class="clients">

    <div class="container">
    
        <ul id="mycarouselthree" class="jcarousel-skin-tango">
          
            <li><img src="images/client-logo/indiana-group-logo.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-thomas.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-galaxy.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-goodluck.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-onerio.jpg" alt=""></li>

            <li><img src="images/client-logo/logo-dudheshwerhealthresort.png" alt=""></li>

            <li><img src="images/client-logo/logo-supriya-life-science.png" alt=""></li>

            <li><img src="images/client-logo/logo-bakul-pharma.jpg" alt=""></li>

          </ul>
    
    </div>

</div><!-- end clients -->

    <div class="clearfix divider_line3"></div>

    <div class="punchline_text_box">
        <div class="left">
        <strong>Want to start something new with us ?</strong>
        <p>The Pratriksh Interior is a perfect blend of a creative agency and the best practices of the digital world. We provide digital promotion services by raising consumer awareness of a product or brand, generating sales, and thus creating a long term brand loyalty.</p>
       
        </div>
        <div class="right"><a href="contact.html" class="knowmore_but"><i class="fa fa-hand-o-right fa-lg"></i> Know&nbsp;More</a></div>
    </div>

     
</div>

</div><!-- end content area -->


<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">

    jQuery(document).ready(function() {
            jQuery('#mycarousel').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouseltwo').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselthree').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselfour').jcarousel();
    });
    
</script>

<!-- testimonials -->
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
    
$(".controlls li a").click(function(e) {
    e.preventDefault();
    var id = $(this).attr('class');
    $('#slider div:visible').fadeOut(500, function() {
        $('div#' + id).fadeIn();
    })
});
});//]]>  

</script>



<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>


</body>
</html>

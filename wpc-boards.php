<?php
   include 'currenturl.php';
?>
<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
    <title>WPC Board - Pratiksha Interior</title>
    
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
    
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
    <link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">

    <!-- testimonials -->
    <link rel="stylesheet" href="js/testimonials/fadeeffect.css" type="text/css" media="all">

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
    
    
    
</head>

<body>

<div class="site_wrapper">
   

<!-- HEADER -->
<?php include 'includes/header.php' ?>
<!-- end header -->

<div class="clearfix"></div>


<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>WPC Board</strong></h1>
        
       
    </div>

</div>


<!-- Contant
======================================= -->

<div class="container">

    <div class="content_fullwidth">

       <div class="one_half">
    
       
       <img src="images/tile-sheet-wpc.png" alt="" class="image_left1">
       
    
        </div>

        <div class="one_half last">
     
         
          <p>WPC Boards are the best alternate for the traditional use of wood and plywood for its durability and thermoplastic for its flexibility. WPC Boards are the overcome of the entire problem faced with the plywood. WPC Board have features such as highly resistant to extreme weather, moisture and termites and low maintenance. After conventional timbers in many areas. The result is that WPC Board Product won't rot, crack, warp or splinter (as long as installed and used as recommended) meaning you can rest easy for years.</p>

           <div class="clearfix mar_top10"></div>
           <div class="clearfix mar_top8"></div>

        </div>


    <div class="clearfix divider_line2"></div>


    <h2><strong>ENVIRONMENT</strong></h2>
    <p>WPC BOARD is leading the way with its environmentally friendly, low maintenance range of thermoplastic composite decking boards. WPC is conserving our precious native forest timbers, and making positive inroads to reducing global warming and habitat degradation. As WPC BOARD requires no painting, staining or oiling and this avoids using potentially polluting preservatives.</p>

       <div class="mar_top4"></div>

     

        <div class="one_third">
            <div class="framed-box">
                <div class="framed-box-wrap">
                    <div class="pricing-title">
                    <h3>Interior Applications</h3>
                    </div>
                    <div class="pricing-text-list">
                        <ul class="list1">
                            <li><i class="fa fa-star-o"></i>Modular Kitchens & Partitions</li>
                            <li><i class="fa fa-star-o"></i>Home & Office Furniture</li>
                            <li><i class="fa fa-star-o"></i>Wall Panelling & Ceiling Solutions</li>
                            <li><i class="fa fa-star-o"></i>Wardrobes & Bathroom Cabinets</li>
                            <li><i class="fa fa-star-o"></i>Industrial Sections</li>
                            <li><i class="fa fa-star-o"></i>Control Cabinets & Panels</li>
                           
                        </ul>
                      
                    </div>
                </div>
                
            </div>
        </div>
        <div class="one_third">
            <div class="framed-box">
                <div class="framed-box-wrap">
                    <div class="pricing-title">
                    <h3>Exterior Applications</h3>
                    </div>
                    <div class="pricing-text-list">
                        <ul class="list1">
                            <li><i class="fa fa-heart"></i>Constructions / Shuttering Boards</li>
                            <li><i class="fa fa-heart"></i>Exterior Wall Cladding</li>
                            <li><i class="fa fa-heart"></i>Garden Furniture & Fencing</li>
                            <li><i class="fa fa-heart"></i>Pre-Fabricated House</li>
                            <li><i class="fa fa-heart"></i>Other Exterior Applications</li>
                            <li><i class="fa fa-heart"></i>Other Exterior Applications</li>
                        
                        </ul>
                        
                    </div>
                </div>
                
            </div>
        </div>

        <div class="one_third last">
            <div class="framed-box">
                <div class="framed-box-wrap">
                    <div class="pricing-title">
                    <h3>For Advertisements</h3>
                    </div>
                    <div class="pricing-text-list">
                        <ul class="list1">
                        
                        <li><i class="fa fa-shopping-cart"></i>Sign Boards</li>
                        <li><i class="fa fa-shopping-cart"></i>Display Boards</li>
                        <li><i class="fa fa-shopping-cart"></i>Direct Digital Printing</li>
                        <li><i class="fa fa-shopping-cart"></i>Exhibitions Stands & Graphics</li>
                        <li><i class="fa fa-shopping-cart"></i>Exhibitions Stands & Graphics</li>
                        <li><i class="fa fa-heart"></i>Other Exterior Applications</li>
                        </ul>
                        
                    </div>
                </div>
                
            </div>
        </div>

 

    <div class="clearfix divider_line8"></div>

   <div class="one_full">
    
       
            <div class="table-style">
                <table class="table-list">
                    <tbody><tr>
                        <th>Available Thickness (PVC FOAM BOARD/WPC BOARD)</th>
                        <th>Density</th>
                        <th>Sizes</th>
                       
                    </tr>
                    <tr>
                        <td>5.0, 6.0, 8.0, 10, 12, 15, 17, 18, 20, 25 (MM)</td>
                        <td>0.55</td>
                        <td>4 feet X 8 Feet(1220 mm X 2440 mm)</td>
                       
                    </tr>
                    
                </tbody></table>

            </div>
    
    </div>

    <p><strong>Note:</strong>The white is our raw material color. The Other colors are derived from pigmentation and pigment are subject to sunlight fading. Above all information are only for good reference, DION is not responsible for any loss from miss uses or misunderstanding of instruction. Company has power to change in specification at any time. For more details, please visit our website.</p>

   
    <div class="content_fullwidth">
    
        <h2><strong>APPLICATIONS</strong></h2>
        <p>INDUSTRIAL, COMMERCIAL, RESIDENTIAL & AGRICULTURAL</p>
          <div class="clearfix mar_top3"></div>
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
           <img src="images/kitchen-1.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/kitchen-2.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/kitchen-3.jpg" alt="">
            
            </div>
        </div><!-- end section -->
        
        <div class="one_fourth last">
            <div class="portfolio_image">
            <i class="fa fa-search fa-2x"></i>
            <img src="images/kitchen-4.jpg" alt="">
            
            </div> 
        </div><!-- end section -->

        
    </div>


    <div class="clearfix divider_line3"></div>

        <div class="clients">

    <div class="container">
    
        <ul id="mycarouselthree" class="jcarousel-skin-tango">
          
            <li><img src="images/client-logo/indiana-group-logo.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-thomas.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-galaxy.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-goodluck.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-onerio.jpg" alt=""></li>

            <li><img src="images/client-logo/logo-dudheshwerhealthresort.png" alt=""></li>

            <li><img src="images/client-logo/logo-supriya-life-science.png" alt=""></li>

            <li><img src="images/client-logo/logo-bakul-pharma.jpg" alt=""></li>

          </ul>
    
    </div>

</div><!-- end clients -->



    <div class="clearfix divider_line3"></div>

    <div class="punchline_text_box">
        <div class="left">
        <strong>Want to start something new with us ?</strong>
        <p>The Pratriksh Interior is a perfect blend of a creative agency and the best practices of the digital world. We provide digital promotion services by raising consumer awareness of a product or brand, generating sales, and thus creating a long term brand loyalty.</p>
       
        </div>
        <div class="right"><a href="contact.html" class="knowmore_but"><i class="fa fa-hand-o-right fa-lg"></i> Know&nbsp;More</a></div>
    </div>

   
</div>

</div><!-- end content area -->


<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">

    jQuery(document).ready(function() {
            jQuery('#mycarousel').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouseltwo').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselthree').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselfour').jcarousel();
    });
    
</script>

<!-- testimonials -->
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
    
$(".controlls li a").click(function(e) {
    e.preventDefault();
    var id = $(this).attr('class');
    $('#slider div:visible').fadeOut(500, function() {
        $('div#' + id).fadeIn();
    })
});
});//]]>  

</script>



<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>

</body>
</html>

<?php
   include 'currenturl.php';
?>

<!doctype html>
 <html lang="en-gb" class="no-js"> 
<head>
    <title>PVC Foam Board - Pratiksha Interior</title>
    
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
  
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- progressbar -->
    <link rel="stylesheet" href="js/progressbar/ui.progress-bar.css">

    <!-- testimonials -->
    <link rel="stylesheet" href="js/testimonials/fadeeffect.css" type="text/css" media="all">

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
    
    
</head>

<body>

<div class="site_wrapper">
   


<?php include 'includes/header.php' ?>


<div class="clearfix"></div>


<div class="parallax_sec1">

    <div class="container">
    
        <h1><strong>PVC Foam Board</strong></h1>
        
       
    </div>

</div>

<div class="container">

    <div class="content_fullwidth">

       <div class="one_half">
    
       <img src="images/tile-sheet.png" alt="" class="image_left1">
        
        </div>

        <div class="one_half last">
     
        <p>PVC foam board/Sheet is a composite material that is used in the advertising, furniture and building industries and it is a substitution of Plywood, Marine Ply and Particle Board. It can be used for making ceiling panels, cabinet doors, decorative wall and door panels. It's made of Poly Vinyl Chloride is moisture-resistant and fire-proof. It lasts long and does not fall prey to termites or corrosion like wood and metal.</p>
        <p>The main material PVC resin can be foamed and extruded into a special sheet with many advantages, such as glossy or matt surface, waterproof, flame-resistant, UV stable, sound and heat insulation, anti-chemical corrosion, etc, it has a very widely applications.
        </p>
         <div class="clearfix mar_top8"></div>
       
        </div>

    <div class="clearfix divider_line2"></div>


    <div class="get_features">

           <h2>Feature of<strong> PVC Foam Board</strong></h2>
    

        <div class="clearfix mar_top3"></div>

            <div class="one_third">
            
                <ul class="get_features_list">
                    <li class="left"><i class="icon-coffee icon-2x"></i></li>
                    <li class="right">
                    <h5>Water Proof, Termite Proof, Borer Proof, Rust Proof, Weather resistant, Fire Retardant</h5>
                    </li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-file-alt icon-2x"></i></li>
                    <li class="right">
                    <h5>Resistant to chemical & corrosion</h5>
                   </li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-fullscreen icon-2x"></i></li>
                    <li class="right">
                    <h5>Easy to work and Process and Suitable for film lamination</h5>
                    </li>
                </ul>
                
            </div>
            
            <div class="one_third">
            
                <ul class="get_features_list">
                    <li class="left"><i class="icon-resize-full icon-2x"></i></li>
                    <li class="right">
                    <h5>Good sound insulation, Good thermal insulation</h5>
                   </li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-th icon-2x"></i></li>
                    <li class="right">
                    <h5>Suitable for both indoor & outdoor application</h5>
                    </li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-crop icon-2x"></i></li>
                    <li class="right">
                    <h5>Light Weight, Smooth and Glossy surface</h5>
                    </li>
                </ul>

            </div>
            
            <div class="one_third last">
            
                 <ul class="get_features_list">
                    <li class="left"><i class="icon-trophy icon-2x"></i></li>
                    <li class="right">
                    <h5>Direct Digital printing printingprinting</h5>
                   </li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><i class="icon-trophy icon-2x"></i></li>
                    <li class="right">
                    <h5>Highly suitable for printing, lacquering fine celled foam structure</h5>
                   </li>
                </ul>
              
                <ul class="get_features_list last">
                    <li class="left"><i class="icon-align-justify icon-2x"></i></li>
                    <li class="right">
                    <h5>3 Layer uPVC foam board, top and bottom is uPVC and middle is foam PVC material.</h5>
                  </li>
                </ul>
                
            </div>
 
        </div>
    

    <div class="clearfix divider_line8"></div>

   <div class="one_full">
    
       
            <div class="table-style">
                <table class="table-list">
                    <tbody><tr>
                        <th>Property</th>
                        <th>Unit</th>
                        <th>Average Result</th>
                       
                    </tr>
                    <tr>
                        <td>Apparent density</td>
                        <td>g/cm</td>
                        <td>0.5-0.8</td>
                       
                    </tr>
                    <tr>
                        <td>Determination of Water Absorption</td>
                        <td>%</td>
                        <td>0.19</td>
                       
                    </tr>
                    <tr>
                        <td>Tensile Strength at Yield</td>
                        <td>Mpa</td>
                        <td>19</td>
                        
                    </tr>

                    <tr>
                        <td>Elongation at Break</td>
                        <td>%</td>
                        <td>16</td>
                        
                    </tr>

                    <tr>
                        <td>Flexural Modulus</td>
                        <td>GPa</td>
                        <td>0.9</td>
                        
                    </tr>

                    <tr>
                        <td>Charpy Impact Strength</td>
                        <td>KJ/m</td>
                        <td>1.4</td>
                        
                    </tr>

                    <tr>
                        <td>Shore D Hardness</td>
                        <td>Value</td>
                        <td>50</td>
                        
                    </tr>

                    <tr>
                        <td>Pulling Strength</td>
                        <td>Mpa</td>
                        <td>12</td>
                        
                    </tr>

                    <tr>
                        <td>Bending Strength</td>
                        <td>Mpa</td>
                        <td>20</td>
                        
                    </tr>

                    <tr>
                        <td>Compressive Strength</td>
                        <td>Mpa</td>
                        <td>4</td>
                        
                    </tr>
                    
                </tbody></table>

            </div>
    
    </div>

    <p><strong>Note:</strong>The information on physical and chemical characteristics is based upon tests believed to be reliable. The values are intended only as a source of information. A legally binding guarantee of specific properties is not to be inferred from our specifications. They are given without guaranty and do not constitute a warranty. The purchaser should independently determine, prior to use, the suitability of this material for his/her specific purpose.</p>

    
    <div class="clearfix divider_line3"></div>


        <div class="clients">

    <div class="container">
    
        <ul id="mycarouselthree" class="jcarousel-skin-tango">
          
            <li><img src="images/client-logo/indiana-group-logo.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-thomas.png" alt=""></li>
            
            <li><img src="images/client-logo/logo-galaxy.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-goodluck.jpg" alt=""></li>
            
            <li><img src="images/client-logo/logo-onerio.jpg" alt=""></li>

            <li><img src="images/client-logo/logo-dudheshwerhealthresort.png" alt=""></li>

            <li><img src="images/client-logo/logo-supriya-life-science.png" alt=""></li>

            <li><img src="images/client-logo/logo-bakul-pharma.jpg" alt=""></li>

          </ul>
    
    </div>

</div><!-- end clients -->

    <div class="clearfix divider_line3"></div>

    <div class="punchline_text_box">
        <div class="left">
        <strong>Want to start something new with us ?</strong>
        <p>The Pratriksh Interior is a perfect blend of a creative agency and the best practices of the digital world. We provide digital promotion services by raising consumer awareness of a product or brand, generating sales, and thus creating a long term brand loyalty.</p>
       
        </div>
        <div class="right"><a href="contact.html" class="knowmore_but"><i class="fa fa-hand-o-right fa-lg"></i> Know&nbsp;More</a></div>
    </div>

     
</div>

</div>

<div class="clearfix mar_top5"></div>

<!-- Footer
======================================= -->

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

 
</div>

<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">

    jQuery(document).ready(function() {
            jQuery('#mycarousel').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouseltwo').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselthree').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselfour').jcarousel();
    });
    
</script>

<!-- testimonials -->
<script type="text/javascript">//<![CDATA[ 
$(window).load(function(){
    
$(".controlls li a").click(function(e) {
    e.preventDefault();
    var id = $(this).attr('class');
    $('#slider div:visible').fadeOut(500, function() {
        $('div#' + id).fadeIn();
    })
});
});//]]>  

</script>



<!-- progress bar -->
<script src="js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-374977-27']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<script type="text/javascript" src="js/sticky-menu/core.js"></script>


</body>
</html>
